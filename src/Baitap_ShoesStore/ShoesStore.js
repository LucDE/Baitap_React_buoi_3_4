import Modal from "./Modal";
import ProductList from "./ProductList";
import React from "react";
import products from "./data.json";
export default class ShoesStore extends React.Component {
  constructor() {
    super();
    this.state = {
      products,
      productDetail: "",
    };
    this.setStateModal = this.setStateModal.bind(this);
  }
  setStateModal(e) {
    const id = e.target.parentNode.parentNode.id;
    this.setState({
      productDetail: this.state.products[id],
    });
  }
  render() {
    const products = this.state.products;
    return (
      <div>
        <ProductList
          productsData={products}
          setStateModal={this.setStateModal}
        />
        <Modal product={this.state.productDetail} />
      </div>
    );
  }
}
