function ProductItem(props) {
  const { name, image, price } = props.item;
  return (
    <div>
      <img
        data-toggle="modal"
        data-target="#modelId"
        className="cursor-pointer"
        src={image}
        alt={name}
        onMouseDown={props.setStateModal}
      ></img>
      <p>{name}</p>
      <p>{price + " $"}</p>
      <button className="bg-black text-white p-1">
        add to carts<i className="fa-solid fa-cart-shopping ml-1"></i>
      </button>
    </div>
  );
}
export default ProductItem;
