function Modal(props) {
  const { name, description, shortDescription, quantity } = props.product;
  const style_h5 = { color: "crimson", fontWeight: "700" };
  return (
    <div
      className="modal hide"
      id="modelId"
      tabIndex={-1}
      role="dialog"
      aria-labelledby="modelTitleId"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title d-inline text-center" style={style_h5}>
              PRODUCT INFORMATION
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body space-y-3">
            <p className="font-bold">
              Name: <i className="text-sm font-normal">{name} </i>
            </p>
            <p className="font-bold">
              Description: <i className="text-sm font-normal">{description}</i>
            </p>
            <p className="font-bold">
              Short description:{" "}
              <i className="text-sm font-normal">{shortDescription}</i>
            </p>
            <p className="font-bold">
              Quantity: <i className="text-sm font-normal">{quantity}</i>
            </p>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary text-dark"
              data-dismiss="modal"
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Modal;
