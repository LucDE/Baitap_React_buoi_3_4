import React from "react";
import ProductItem from "./ProductItem";
// import ProductItem from "./ProductItem";
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productsData: this.props.productsData,
    };
    this.setStateModal = this.setStateModal.bind(this);
  }
  setStateModal(e) {
    this.props.setStateModal(e);
  }
  render() {
    const products = this.state.productsData;
    return (
      <div className="mx-auto max-w-7xl grid grid-cols-3 gap-7">
        {products &&
          products.map((product, index) => {
            return (
              <div key={index} className="border-2 p-3" id={index}>
                <ProductItem
                  item={product}
                  setStateModal={this.setStateModal}
                />
              </div>
            );
          })}
      </div>
    );
  }
}
