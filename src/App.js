import "./App.css";
import ShoesStore from "./Baitap_ShoesStore/ShoesStore";
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="p-5 text-center font-normal text-3xl">Shoes shop</h1>
      </header>
      <ShoesStore />
    </div>
  );
}

export default App;
